console.log("Helloooooooo");


// #1
function getConvertWord(word){
	let convertWord = word.split('');
	let sortWord = convertWord.sort();
	let arrayWord = sortWord.toString();
	console.log(arrayWord);
};

getConvertWord('webmaster');


// #2
let phrase = 'The quick brown fox';

function countVowel(){
	let letters = phrase.split('');

	let vowel = letters.filter(function(letter){
		if (letter == "a" || letter == "e" || letter == "i" || letter == "o" || letter == "u"){
			return true;
		};
	}).length;
	console.log(vowel);
};

countVowel();

// #3
let countries = ["Philippines", "Indonesia", "Vietnam", "Thailand"];

function addCountry(){
	let addToArr = countries.unshift("Brunei", "Cambodia", "Myanmar", "Timor-Leste", "Laos", "Malaysia", "Singapore");
	console.log(countries.sort());
}

addCountry();


// #4
let person = {
	firstName: "Janie",
	lastName: "Jardeleza",
	age: 27,
	gender: "Female",
	nationality: "Filipino"
};

console.log(person);


// #5
function Aslan(){
	this.name = "Aslan";
	this.species = "Lion";
	this.aka = "The Great Lion";
	this.home = "Aslan's Country";
	this.quote = function(){
		console.log(`"But there I have another name. You must learn to know me by that name. This was the very reason you were brought into Narnia, that by knowing me here for a little, you might know me better there."	― ${this.name}`)
	}
}

function MrTumnus(){
	this.name = "Mr. Tumnus";
	this.species = "Faun";
	this.aka = "Master Tumnus";
	this.home = "Narnia";
	this.quote = function(){
		console.log(`"This is the land of Narnia, where we are now; all that lies between the lamppost and the great castle of Cair Paravel on the Eastern Sea. And you--you have come from the wild woods of the west?"	― ${this.name}`)
	}
}

function JadisWitch(){
	this.name = "Jadis";
	this.species = "Faun";
	this.aka = "White Witch";
	this.home = "Charn";
	this.quote = function(){
		console.log(`"Then you'll remember well that every traitor belongs to me. His blood is my property."	― ${this.name}`)
	}
}

let aslan = new Aslan();
let tumnus = new MrTumnus();
let witch = new JadisWitch();

aslan.quote();
tumnus.quote();
witch.quote();